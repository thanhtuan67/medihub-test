// Libraries
import 'bootstrap/dist/js/bootstrap.bundle'
import 'jquery/dist/jquery'
import 'jquery/dist/jquery.slim'

// Pages
import './header'
import './product-detail'
import './main'
import './cart'
import './categories'






