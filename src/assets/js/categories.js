var filterList = document.querySelector('.filter-list')

// Show bảng lọc sản phẩm
if (filterList != null) {
  let btn = document.getElementById('filter-popup')
  let filter = document.getElementById('filter')
  let overlay = document.querySelector('.overlay-filter')
  let close = document.querySelector('.close-filter')
  let inner = document.getElementById('inner-filter')

  btn.addEventListener('click', menuActive)
  overlay.addEventListener('click', menuInactive)
  close.addEventListener('click', menuInactive)

  function menuActive() {
    filter.style.visibility = "visible"
    inner.style.transform = "translateX(0px)"
    overlay.style.opacity = 1
  }

  function menuInactive() {
    filter.style.visibility = "hidden"
    inner.style.transform = "translateX(100%)"
    overlay.style.opacity = 0
  }
}