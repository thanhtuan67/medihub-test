import $ from 'jquery'
import 'slick-carousel'

// Slider trang sản phẩm chi tiết
$('.slider-single').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  asNavFor: '.slider-nav'
});

$('.slider-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: false,
  asNavFor: '.slider-single',
  dots: false,
  centerMode: false,
  focusOnSelect: true
});
