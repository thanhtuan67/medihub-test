import $ from 'jquery'

// Show navigation bar - thanh menu (dưới dạng mobile)
var headerMenu = document.querySelector('.header-menu-list')

if (headerMenu != null) {
  let btnMenu = document.getElementById('menuPopup')
  let menu = document.getElementById('menu')
  let overlay = document.querySelector('.overlay')
  let inner = document.getElementById('inner')

  btnMenu.addEventListener('click', filterActive)
  overlay.addEventListener('click', filterInactive)

  function filterActive() {
    menu.style.visibility = "visible"
    inner.style.transform = "translateX(0px)"
    overlay.style.opacity = 1
  }

  function filterInactive() {
    menu.style.visibility = "hidden"
    inner.style.transform = "translateX(-100%)"
    overlay.style.opacity = 0
  }
}

// Tạo hover cho mục sản phẩm thanh navigation bar 
var navBox = document.querySelector(".nav-box")
var show = document.getElementById("categories-show")

if (navBox != null) {
  show.addEventListener("mousemove", cateOn)
  show.addEventListener("mouseout", cateOff)
  
  function cateOn () {
    navBox.style.display = "block"
  }
  
  function cateOff () {
    navBox.style.display = "none"
  }
}


