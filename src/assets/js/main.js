import $ from 'jquery'
import 'slick-carousel'

$('.banner-slick').slick({
  dots: false,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 3000,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1
})